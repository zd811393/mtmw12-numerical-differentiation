# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 14:29:07 2019

@author: zd811393
"""

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *

def geostrophicWind100():
    # Input parameters describing the problem
    import geoParameters as gp
    
    # Resolution
    N = 100          # the number of intervals to divide space into
    dy = (gp.ymax - gp.ymin)/N # the length of the spacing
    
    # The spatial dimension, y:
    y = np.linspace(gp.ymin, gp.ymax, N+1)
    
    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)
    
    #The pressure at the y points
    p = gp.pressure(y)
    
    #The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = gp.geoWind(dpdy)
    Error = abs(u_2point - uExact)
    
    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size' : 14}
    plt.rc('font', **font)
    
    # Plot the approximate and exact wind at y points
    plt.figure(0)
    plt.plot(y/1000, uExact, 'k-', label='Exact')
    plt.plot(y/1000, u_2point, '*k-', label='Two-point differences', \
             ms=12,markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    
    # Plot of error in u_2point
    plt.figure(1)
    plt.plot(y/1000, Error, 'k-', label = 'Error, N = 100')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('Difference (m/s)')
    
    
    plt.tight_layout()
    plt.savefig('plots/geoWindCent.pdf')
    plt.show()
    
    
   
    
    
    
    
if __name__ == "__main__":
    geostrophicWind100()
        