# -*- coding: utf-8 -*-
"""
Created on Fri Oct 18 15:20:07 2019

@author: zd811393
"""

import numpy as np

# Functions for calculating gradients

def gradient_4point(f, dx):
    """The gradient of array f assuming points are a distance dx apart
    using 4-point differences for all points bar the first 2 and last 2 terms,
    and 3-point difference for the 2nd and penultimate terms"""
    
    
    
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Two point differences at the end points
    dfdx[0] = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    # Three point difference for end point adjacent terms
    dfdx[1] = (-f[3] + 6*f[2] - 3*f[1] - 2*f[0])/(6*dx)
    dfdx[-2] = (2*f[-1] + 3*f[-2] - 6*f[-3] + f[-4])/(6*dx)
    
    # Centred differneces for the mid-points
    for i in range(2,len(f)-2):
        dfdx[i] = (-f[i+2] + 8*f[i+1] - 8*f[i-1] + f[i-2])/(12*dx)
    return dfdx